% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/edges.R
\name{Edgelist.reverse}
\alias{Edgelist.reverse}
\title{Edgelist reversal}
\usage{
Edgelist.reverse(edgelist)
}
\arguments{
\item{edgelist}{an Edgelist object}
}
\value{
an Edgelist with same nodes as \code{edgelist} but reversed edges
}
\description{
Retrieves an Edgelist with reversed edges from an Edgelist given as input
}
\examples{
#Build an Edgelist with all (X, Y) such that X in {"A", "B", "C"} and Y in {"D", "E"}
edgelist = Edgelist.bipartite.all(c("A", "B", "C"), c("D", "E"))
#Get the reversed Edgelist which then have the following edgeIds: c(16, 21, 17, 22, 18, 23)
#Corresponds to all (X, Y) such that X in {"D", "E"} and Y in {"A", "B", "C"}
revEdgelist = Edgelist.reverse(edgelist)

}
