#TestThat
library(testthat)
library(devtools)

D = do.call("rbind", lapply(1:5, function(i){rnorm(5)}))
colnames(D) = paste0("gene", 1:5)



test_that("calculateClique_1", {
 res = calculateClique(3, colnames(D))
 expect_equal(length(res), choose(5, 3))
})

